package fr.webscraping.scraping;

import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.scheduler.Scheduled;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import fr.webscraping.scraping.client.JobAdvert;
import org.jboss.logging.Logger;

@ApplicationScoped
public class WebScraping {
    public static final String JUNGLE_URL = "https://www.welcometothejungle.com/fr/jobs";
    private static final Logger LOGGER = Logger.getLogger(WebScraping.class);

    @Inject
    @Channel("jobs")
    Emitter<JobAdvert> emitter;

    @Scheduled(every = "{frequency}") 
    void scrapData() {
      try {
        Document doc = Jsoup.connect(JUNGLE_URL).get();
        final Elements elements = doc.getElementsByTag("article");

        for (Element element : elements) {
          try {
            final String position = element.getElementsByTag("h3").text();
            final String company = element.getElementsByTag("h4").text();
            final Elements infos = element.getElementsByTag("li");
            final String contract = infos.eq(0).last().getElementsByTag("span").last().text();
            final String location = infos.eq(1).last().getElementsByTag("span").last().text();
            final String date = infos.eq(2).last().getElementsByTag("span").last().text();

            emitter.send(new JobAdvert(position, company, contract, location, date));
          } catch (NullPointerException  | IllegalStateException e) {
            LOGGER.error(e.getMessage(), e);
          }
        }
      } catch (IOException e) {
        LOGGER.error(e.getMessage(), e);
      }
    }
}